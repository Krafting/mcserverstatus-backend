# Minecraft Server Status Backend

PHP Backend for [Minecraft Server Status](https://addons.mozilla.org/en-US/firefox/addon/minecraft-server-status/) Firefox Extension 

# How to Use / Install ?

Git clone this repo it on an online FTP and then replace the link in the extenssion settings by your own link (only the folder, not a specifix file)

**Example**: https://site.org/path/to/mcserverstatus-backend/

*Important: don't forget the trailing slash '/'*

*If you make a mistake you can leave the input form blank, it will reset to the default url*
